﻿unit UnPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.StdCtrls,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, FuncoesDLL,
  IdExplicitTLSClientServerBase, IdFTP, Zip, Vcl.Buttons, Data.DB, Vcl.Grids,
  Vcl.DBGrids;

type
  TFormPrincipal = class(TForm)
    Panel1: TPanel;
    PanelTitulo: TPanel;
    RichEdit1: TRichEdit;
    ProgressBar1: TProgressBar;
    IdHTTP1: TIdHTTP;
    Panel2: TPanel;
    Panel3: TPanel;
    bbAtualizar: TBitBtn;
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure conectaExterno;
    procedure downloadThunder;
    procedure IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    function UnZipFile(ArchiveName, Path: String): boolean;
    procedure instalaThunder;
    procedure BuscaVersaoThunder;
    procedure bbAtualizarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPrincipal: TFormPrincipal;

  Servidor: String;
  Porta: String;
  Usuario: String;
  Senha: String;
  Banco: String;

  CodigoEmpresaLogada: Integer;
  CodigoFuncionarioLogado: Integer;

  UsuarioLogado: String;
  Outros: String;

  versao: string;
  precisaAtualizarThunder: Boolean;

  versaoDownload: integer;
  versaoThunder: integer;
  nome: string;
  dirInstalacao: string;



implementation

uses
UnDM;

{$R *.dfm}

procedure TFormPrincipal.instalaThunder;

     procedure updateDB;
     var
     podeAtualizar: Boolean;
     begin
          DM.ExecutarSQL.Close;
          DM.ExecutarSQL.SQL.Clear;
          DM.ExecutarSQL.SQL.Add('select * from usuonline');
          DM.ExecutarSQL.Open;

          if not DM.ExecutarSQL.IsEmpty
             then
                 begin
                      RichEdit1.Lines.Add('');
                      RichEdit1.Lines.Add('** Atualização cancelada.');
                      RichEdit1.Lines.Add('** Existem usuários logados no sistema.');
                      RichEdit1.Lines.Add('** Solicite que eles fechem o sistema antes de realizar a atualização.');
                      abort;
                 end;

          conectaExterno;
          DM.ExecutarSQLRemoto.Close;
          DM.ExecutarSQLRemoto.SQL.Clear;
          DM.ExecutarSQLRemoto.SQL.Add('select relscript from release where relcod > :0');
          DM.ExecutarSQLRemoto.Params[0].asinteger := versaoThunder;
          DM.ExecutarSQLRemoto.Open;

          //Mensagem(DM.ExecutarSQLRemoto.FieldByName('relscript').AsString,informacao );

          DM.ExecutarSQLRemoto.First;

          while not DM.ExecutarSQLRemoto.Eof do
          begin
               try
                  DM.ExecutarSQL.Close;
                  DM.ExecutarSQL.SQL.Clear;
                  //DM.ExecutarSQL.SQL.Add(DM.ExecutarSQLRemoto.FieldByName('relscript').AsWideString);
                  DM.ExecutarSQL.SQL.Add('do $$');
                  DM.ExecutarSQL.SQL.Add('begin');
                  DM.ExecutarSQL.SQL.Add(DM.ExecutarSQLRemoto.FieldByName('relscript').AsString);
                  DM.ExecutarSQL.SQL.Add('end;');
                  DM.ExecutarSQL.SQL.Add('$$');
                  DM.ExecutarSQL.ExecSQL;
               Except
                     On E: Exception do
                        begin
                             Mensagem(E.Message,Informacao);
                             abort;
                        end;
               end;
               DM.ExecutarSQLRemoto.Next;
          end;
     end;

begin
     RichEdit1.Lines.Add('');
     RichEdit1.Lines.Add('** Iniciando instalação do Thunder ERP...');

     updateDB;
     try
        UnZipFile(nome,dirInstalacao);
     finally
            RichEdit1.Lines.Add('');
            RichEdit1.Lines.Add('** Instalação concluida com sucesso, feche o Atualizador Thunder e abra o Thunder ERP.');
     end;

end;



procedure TFormPrincipal.bbAtualizarClick(Sender: TObject);
begin
     BuscaVersaoThunder;
     RichEdit1.Lines.Clear;
     RichEdit1.Lines.Add('');
     RichEdit1.Lines.Add('** Conectando com os servidores da BrSistemas...');
     RichEdit1.Lines.Add('');
     RichEdit1.Lines.Add('** Procurando atualizações...');

     RichEdit1.Lines.Add('');
     RichEdit1.Lines.Add('** Procurando atualização para o sistema Thunder ERP...');
     if precisaAtualizarThunder
        then
            if Mensagem('Existe uma versão mais nova para o Sistema ThunderERP, gostaria de fazer o download?',Confirmacao)=mrYes
               then
                   begin
                        RichEdit1.Lines.Add('');
                        RichEdit1.Lines.Add('** Realizando Download das atualizações Thunder...');
                        downloadThunder;
                   end
               else
                   begin
                        RichEdit1.Lines.Add('');
                        RichEdit1.Lines.Add('** Atualização cancelada.');
                   end;
end;

procedure TFormPrincipal.BuscaVersaoThunder;
begin
     DM.ExecutarSQL.Close;
     DM.ExecutarSQL.SQL.Clear;
     DM.ExecutarSQL.SQL.Add('select confrelease,confdirinst from conf where confempparcod = :0');
     DM.ExecutarSQL.Params[0].AsInteger := CodigoEmpresaLogada;
     DM.ExecutarSQL.Open;

     versaoThunder  := DM.ExecutarSQL.FieldByName('confrelease').AsInteger;
     dirInstalacao  := DM.ExecutarSQL.FieldByName('confdirinst').AsString+'\thunder\';

     conectaExterno;
     DM.ExecutarSQLRemoto.Close;
     DM.ExecutarSQLRemoto.SQL.Clear;
     DM.ExecutarSQLRemoto.SQL.Add('select max(relcod) as max_release from release');
     DM.ExecutarSQLRemoto.Open;

     if versaoThunder < DM.ExecutarSQLRemoto.FieldByName('max_release').AsInteger
        then
            precisaAtualizarThunder := True;
end;

procedure TFormPrincipal.FormCreate(Sender: TObject);
begin
     //Parâmetros do chamar rotina
     Servidor:=ParamStr(1);
     Porta:=ParamStr(2);
     Usuario:=ParamStr(3);
     Senha:=ParamStr(4);
     Banco:=ParamStr(5);
     CodigoEmpresaLogada:=StrToInt(ParamStr(6));
     CodigoFuncionarioLogado:=StrToInt(ParamStr(7));
     UsuarioLogado:=ParamStr(8);
     Outros:=ParamStr(9);

     //Conexão
     DM.FDConnection1.Connected:=false;
     DM.FDPhysPgDriverLink1.VendorHome:='';;
     DM.FDPhysPgDriverLink1.VendorLib:=ExtractFilePath(Application.ExeName)+'libpq.dll';
     DM.FDPhysPgDriverLink1.Release;


     DM.FDConnection1.Params.Values['server']      :=Servidor;
     DM.FDConnection1.Params.Values['port']        :=Porta;
     DM.FDConnection1.Params.Values['user_name']   :=Usuario;
     DM.FDConnection1.Params.Values['password']    :=Senha;
     DM.FDConnection1.Params.Values['database']    :=Banco;

     DM.FDConnection1.Connected := true;
end;

procedure TFormPrincipal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      case key of
           VK_F5:
                 begin
                       DM.FDQueryUsu.Close;
                       DM.FDQueryUsu.SQL.Clear;
                       DM.FDQueryUsu.SQL.Add('select ');
                       DM.FDQueryUsu.SQL.Add('case when usuonlineusuparcod is not null then cast(''online'' as varchar) else cast(''offline'' as varchar) end as online,');
                       DM.FDQueryUsu.SQL.Add('parcod,');
                       DM.FDQueryUsu.SQL.Add('pardes,');
                       DM.FDQueryUsu.SQL.Add('usulogin');
                       DM.FDQueryUsu.SQL.Add('from usu');
                       DM.FDQueryUsu.SQL.Add('join par on usufunparcod = parcod');
                       DM.FDQueryUsu.SQL.Add('join usuonline on usuonlineusuparcod = usufunparcod');
                       DM.FDQueryUsu.SQL.Add('where parsit = ''A''');
                       DM.FDQueryUsu.SQL.Add('order by online,pardes');
                       DM.FDQueryUsu.Open;
                 end;
      end;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
begin
     DM.FDQueryUsu.Close;
     DM.FDQueryUsu.SQL.Clear;
     DM.FDQueryUsu.SQL.Add('select ');
     DM.FDQueryUsu.SQL.Add('case when usuonlineusuparcod is not null then cast(''online'' as varchar) else cast(''offline'' as varchar) end as online,');
     DM.FDQueryUsu.SQL.Add('parcod,');
     DM.FDQueryUsu.SQL.Add('pardes,');
     DM.FDQueryUsu.SQL.Add('usulogin');
     DM.FDQueryUsu.SQL.Add('from usu');
     DM.FDQueryUsu.SQL.Add('join par on usufunparcod = parcod');
     DM.FDQueryUsu.SQL.Add('join usuonline on usuonlineusuparcod = usufunparcod');
     DM.FDQueryUsu.SQL.Add('where parsit = ''A''');
     DM.FDQueryUsu.SQL.Add('order by online,pardes');
     DM.FDQueryUsu.Open;
end;

function TFormPrincipal.UnZipFile(ArchiveName, Path: String): boolean;
var Zip:TZipFile;
begin
     Zip:=TZipFile.Create;
     try
        zip.Open(ArchiveName,zmRead);
        zip.ExtractAll(Path);
        zip.Close;
        result:=true;
     except
           result:=false;
     end;
     zip.Free;
end;

procedure TFormPrincipal.downloadThunder;
var
   lStrParamentros:string;
   fileDownload : TFileStream;

begin
     try
        nome := 'Thunder.zip';
        fileDownload := TFileStream.Create(nome, fmCreate);
        IdHTTP1.Get('http://191.252.182.93/download/'+nome, fileDownload);
     finally
            FreeAndNil(fileDownload);
            instalaThunder;
     end;

end;

procedure TFormPrincipal.conectaExterno;
begin
     DM.FDDBRemoto.Connected:=false;
     try
        DM.FDDBRemoto.Params.Values['server']      :='191.252.182.93';
        DM.FDDBRemoto.Params.Values['port']        :='5432';
        DM.FDDBRemoto.Params.Values['user_name']   :='postgres';
        DM.FDDBRemoto.Params.Values['password']    :='Ym5.1TkP';
        DM.FDDBRemoto.Params.Values['database']    :='dbbr_atualizacao';
        DM.FDDBRemoto.Connected:=true;
     Except
           RichEdit1.Lines.Add('');
           RichEdit1.Lines.Add('** Erro ao conectar com os servidores da BrSistemas **');
           abort;
     end;
end;

procedure TFormPrincipal.IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
     ProgressBar1.Position := AWorkCount;
end;

procedure TFormPrincipal.IdHTTP1WorkBegin(ASender: TObject;
  AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
     ProgressBar1.Max := AWorkCountMax;
end;

procedure TFormPrincipal.IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
     RichEdit1.Lines.Add('');
     RichEdit1.Lines.Add('** Download das atualizações do Thunder ERP concluídas...');

end;

end.
