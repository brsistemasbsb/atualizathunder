object DM: TDM
  OldCreateOrder = False
  Height = 315
  Width = 531
  object FDConnection1: TFDConnection
    Params.Strings = (
      'User_Name=postgres'
      'Server=191.252.182.93'
      'Database=dbdesenvolvimento'
      'Password=Ym3.1TkP'
      'DriverID=PG')
    Left = 32
    Top = 16
  end
  object ExecutarSQL: TFDQuery
    CachedUpdates = True
    Connection = FDConnection1
    FetchOptions.AssignedValues = [evUnidirectional]
    Left = 32
    Top = 144
  end
  object FDPhysPgDriverLink1: TFDPhysPgDriverLink
    VendorLib = 'libpq.dll'
    Left = 123
    Top = 16
  end
  object OpenDialog1: TOpenDialog
    Left = 472
    Top = 9
  end
  object FDPhysPgDriverLinkRemoto: TFDPhysPgDriverLink
    VendorLib = 'libpq.dll'
    Left = 120
    Top = 74
  end
  object FDDBRemoto: TFDConnection
    Params.Strings = (
      'Database=dbstbarbara'
      'User_Name=postgres'
      'Password=Ym3.1TkP'
      'Server=179.95.5.218'
      'DriverID=PG')
    FetchOptions.AssignedValues = [evAutoClose]
    ResourceOptions.AssignedValues = [rvPersistent]
    LoginPrompt = False
    Left = 32
    Top = 72
  end
  object ExecutarSQLRemoto: TFDQuery
    CachedUpdates = True
    Connection = FDDBRemoto
    FetchOptions.AssignedValues = [evUnidirectional]
    Left = 128
    Top = 144
  end
  object FDQueryUsu: TFDQuery
    CachedUpdates = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      
        #9'case when usuonlineusuparcod is not null then cast('#39'online'#39' as ' +
        'varchar) else cast('#39'offline'#39' as varchar) end as online,'
      #9'parcod,'
      #9'pardes,'
      #9'usulogin'
      'from usu'
      'join par on usufunparcod = parcod '
      'left join usuonline on usuonlineusuparcod = usufunparcod '
      'where parsit = '#39'A'#39
      'order by pardes')
    Left = 272
    Top = 23
    object FDQueryUsuonline: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'online'
      Origin = 'online'
      ReadOnly = True
      Size = 8190
    end
    object FDQueryUsuparcod: TIntegerField
      FieldName = 'parcod'
      Origin = 'parcod'
    end
    object FDQueryUsupardes: TWideStringField
      FieldName = 'pardes'
      Origin = 'pardes'
      Size = 120
    end
    object FDQueryUsuusulogin: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'usulogin'
      Origin = 'usulogin'
      Size = 15
    end
  end
  object DsUsu: TDataSource
    DataSet = FDQueryUsu
    Left = 216
    Top = 23
  end
end
